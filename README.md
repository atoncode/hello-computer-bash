# Hello computer - A Bash script

Un script bash pour discuter de la pluie et du beau temps avec son ordinateur à son démarrage. Pas vraiment un chatbot, juste un petit script amusant. Libre à vous de l'adapter !

A Bash script to chit-chat with your computer at startup. Not really a chatbot, just a funny little script! Feel free to use it! [in French]