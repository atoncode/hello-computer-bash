#!/bin/bash
# Hello ordi !

#Fonctions principales :

main(){

	DATE=$(date +%A\ %d\ %B)
	HEURE=$(date +%Hh%M)
	TIME=$(date +%H)
	UNAME=$(uname -a)

	echo -e "\n\n\tHello $USER !\n\n\t\t\c"
	read REPONSE
	echo -e "\n\tNous sommes le $DATE. Il est $HEURE.
	\n\tJ'exécute $UNAME."

		#Appel des fonctions selon le moment de la journée :
		if [ $TIME -le 6 ]
		then
			nuit #appel fonction
		elif [ $TIME -gt 6 ] && [ $TIME -lt 18 ]
		then
			jour #appel fonction
		else
			soir #appel fonction
		fi

}

nuit(){

        echo -e "\n\tEncore debout $USER ? Il est temps d'aller dormir ! M'éteindre ? (o/n)\n\n\t\t\c"
        read REPONSE
        if [ $REPONSE = o ] || [ $REPONSE = O ]
	then
		extinction #appel fonction			
	else
                echo -e "\n\tNe tarde pas trop quand même.\n\n\t\t\c"
                read REPONSE
		case "$REPONSE" in
                	*lancer*)
                                commandes_nuit #appel fonction
				;;
                        *exécuter*)
                               	commandes_nuit #appel fonction
                               	;;
			*)
                                echo -e "\n\tVeux-tu lancer un programme ? (o/n) \n\n\t\t\c"
                                read REPONSE
	                        if [ $REPONSE = o ] || [ $REPONSE = O ] || [ $REPONSE = "Oui" ] || [ $REPONSE = "oui" ]
                                then
                                	commandes_nuit #appel fonction
				else
					echo -e "\n\tD'acc'. Bon courage et bonne nuit !\n\n"
                                        bash
                                fi
                esac
	fi

}

jour(){

        echo -e "\n\tComment vas-tu aujourd'hui ?\n\n\t\t\c"
        read
        echo -e "\n\tBien, bien, merci. Veux-tu lancer un programme ? (o/n)\n\n\t\t\c"
        read REPONSE
                if [ $REPONSE = o ] || [ $REPONSE = O ] || [ $REPONSE = "Oui" ] || [ $REPONSE = "oui" ]
                then
                        echo -e "\n\tAlors entre une commande : \n\n\t\t\c"
                        read REPONSE
                        $REPONSE &
                        echo -e "\n\tVoilà. Passe une bonne journée ! :-)\n\n"
                        bash
                else
                        echo -e "\n\tOK. Passe une bonne journée ! :-)\n\n\t\t\c"
                        read
                        echo -e "\n\n"
                        bash
                fi

}

soir(){

	echo -e "\n\tAs-tu passé une bonne journée ?\n\n\t\t\c"
       	read
        echo -e "\n\tBien merci. Que veux-tu faire ?\n\n\t\t\c"
        read REPONSE
                case "$REPONSE" in
                	Lancer*)
				commandes_soir #appel fonction
                		;;
                	*lancer*)
				commandes_soir #appel fonction
                		;;
                	Exécuter*)
				commandes_soir #appel fonction
                		;;
                	*exécuter*)
				commandes_soir #appel fonction
                		;;
                	*)
                        	echo -e "\n\tDans ce cas passe une bonne soirée, bon courage si tu travailles. Si non amuse-toi bien et ne te couche pas trop tard !\n\n\t\t\c"
                        	read
                        	echo -e "\n\n"
                        	bash
				;;
                esac

}

#Fonctions secondaires :

commandes_soir(){

	echo -e "\n\tAlors entre une commande : \n\n\t\t\c"
	read REPONSE
	$REPONSE &
	echo -e "\n\tVoilà. Passe une bonne soirée ! :-)\n\n"
	bash

}

commandes_nuit(){

	echo -e "\n\tAlors entre une commande : \n\n\t\t\c"
        read REPONSE
        $REPONSE &
        echo -e "\n\tVoilà. Bon courage ! :-)\n\n"
        bash
        
}

extinction(){
	
	echo -e "\n\tJe vais maintenant m'éteindre. Bonne nuit et à demain !\n\n\t\t\c"
        read
        echo -e "\n\tAttention !\n\n\t\t\c"
        #Décompte avant extinction :	
	for ((i=10;i>0;i-=1))
        do
	        sleep 1
                echo $i
        done
        shutdown -h now

}

main #appel fonction
